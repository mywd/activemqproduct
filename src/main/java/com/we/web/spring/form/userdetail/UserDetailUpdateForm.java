/**
 * UserDetailUpdateForm.java
 * com.we.web.spring.form.userdetail
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.form.userdetail;

import lombok.Data;

/**
 * 用户详情更新表单
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月24日
 */
@Data
public class UserDetailUpdateForm {
	/**
	 * 名称
	 */
	private String name;
}
