/**
 * UserViewForm.java
 * com.we.web.spring.form
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.form.user;

import lombok.Data;

import com.we.core.db.form.IEntityForm;
import com.we.core.db.form.support.Condition;

/**
 * 用户列表表单
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月10日
 */
@Data
public class UserListForm implements IEntityForm {
	/**
	 * 名称
	 */
	@Condition
	private String name;
}
