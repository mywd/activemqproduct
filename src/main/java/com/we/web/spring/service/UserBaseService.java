/**
 * WebUserService.java
 * com.we.web.spring.service
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service;

import org.springframework.stereotype.Service;

import com.we.core.db.service.BaseService;
import com.we.web.spring.dao.UserDao;
import com.we.web.spring.entity.UserEntity;

/**
 * 用户基础服务
 * 
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月6日
 */
@Service
public class UserBaseService extends BaseService<UserDao, UserEntity> {
}
