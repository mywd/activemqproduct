/**
 * MyQueueProducter.java
 * com.we.web.spring.service.mq
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.service.mq;

import javax.annotation.Resource;

import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;

/**
 * 队列生产者
 *
 * @author   MiaoYang(147621629@qq.com)
 * @Date	 2017年8月2日 	 
 */
@Service
public class MyQueueProducterService {
	//JmsTemplate为JmsOperations的具体实现,一般注入接口解耦
	@Resource(name = "jmsQueueTemplate")
	private JmsOperations jmsQueueTemplate;

	/**
	 * 发送消息
	 *
	 * @param message 消息
	 */
	public void sendMessage(String message) {
		jmsQueueTemplate.convertAndSend(message);
		System.out.println("发送了消息到myQueue:" + message);
	}
}
