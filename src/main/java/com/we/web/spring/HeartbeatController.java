/**
 * MainController.java
 * com.we.web.spring
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 心跳控制器
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月10日
 */
@Controller
public class HeartbeatController {

	@ResponseBody
	@RequestMapping
	public Object alive() {
		return "I'm alive!!! Don't worry ^_^";
	}
}
