/**
 * UserViewTag.java
 * com.we.web.spring.vt
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.vt;

import java.util.Map;

import org.nutz.lang.Mirror;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.we.core.common.util.ConvertUtil;
import com.we.core.common.util.MapUtil;
import com.we.core.common.util.Util;
import com.we.core.vt.impl.AbstractCacheViewTag;
import com.we.core.web.util.FtlUtil;
import com.we.web.spring.entity.UserEntity;
import com.we.web.spring.service.UserBaseService;

import freemarker.template.Configuration;

/**
 * 用户视图标签
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月12日
 */
@Component
public class UserViewTag extends AbstractCacheViewTag {
	@Autowired
	private UserBaseService userBaseService;
	@Autowired
	private Configuration configuration;

	@Override
	protected String loadFormDb(final Map<String, String> params) {
		long userId = ConvertUtil.obj2long(params.get(UID));
		if (Util.isEmpty(userId)) {
			return "";
		}

		String subType = params.get(SUB_TYPE);
		if (Util.isEmpty(subType)) {
			return "";
		}
		UserEntity user = userBaseService.get(userId);
		if (Util.isEmpty(user)) {
			return "";
		}
		Map<String, Object> map = MapUtil.map();
		map.put("user", user);
		switch (subType) {
		case "text":
			map.put("text", Mirror.me(user).getValue(user, params.get("name")));
			break;

		default:
			break;
		}
		return FtlUtil.build(configuration, getFtlPath(subType), map);
	}

	@Override
	protected String getType() {
		return "user";
	}

	@Override
	protected int getExpireTime() {
		return 1;
	}

}
