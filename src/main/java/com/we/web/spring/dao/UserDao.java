/**
 * WebUserDao.java
 * com.we.web.spring.dao
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.dao;

import com.we.core.db.base.BaseMapper;
import com.we.web.spring.entity.UserEntity;

/**
 * 用户dao
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年4月6日
 */
public interface UserDao extends BaseMapper<UserEntity> {
}
