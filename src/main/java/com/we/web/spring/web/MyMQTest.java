/**
 * MyMQTest.java
 * com.we.web.spring.web
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.we.web.spring.service.mq.MyQueueProducterService;
import com.we.web.spring.service.mq.MyTopicProducterService;

/**
 *
 * @author   MiaoYang(147621629@qq.com)
 * @Date	 2017年8月2日 	 
 */
@Controller
@RequestMapping("/my")
public class MyMQTest {
	@Autowired
	private MyQueueProducterService myQueueProducter;

	@Autowired
	private MyTopicProducterService myTopicProducterService;

	@RequestMapping
	@ResponseBody
	public void sendMessagesToQueue(String message) {
		myQueueProducter.sendMessage(message);
	}

	@RequestMapping
	@ResponseBody
	public void sendMessagesToTopic(String message) {
		myTopicProducterService.sendMessage(message);
	}
}
