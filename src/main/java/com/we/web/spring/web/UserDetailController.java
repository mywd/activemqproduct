/**
 * UserDetailController.java
 * com.we.web.spring.web
 * Copyright (c) 2017, 北京聚智未来科技有限公司版权所有.
*/

package com.we.web.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.we.core.db.page.Page;
import com.we.web.spring.form.userdetail.UserDetaiDetailForm;
import com.we.web.spring.form.userdetail.UserDetailListForm;
import com.we.web.spring.form.userdetail.UserDetailUpdateForm;
import com.we.web.spring.service.UserDetailViewService;

/**
 * 用户详情控制器
 * <p>
 * 适用于多表操作
 *
 * @author   ZhuangJunxiang(529272571@qq.com)
 * @Date	 2017年7月6日
 */
@Controller
@RequestMapping("/userdetail")
public class UserDetailController {
	@Autowired
	private UserDetailViewService userDetailViewService;

	/**
	 * 列表
	 * 
	 * @param userDetailListForm 用户列表表单
	 * @param page 分页
	 * @param model 模型
	 */
	@RequestMapping
	public void list(@ModelAttribute final UserDetailListForm userDetailListForm, @ModelAttribute final Page page,
			final Model model) {
		userDetailViewService.setListInfo(userDetailListForm, page, model);
	}

	/**
	 * 实体详情
	 * 
	 * @param userDetaiDetailForm 用户列表表单
	 */
	@RequestMapping
	public void detail(@ModelAttribute final UserDetaiDetailForm userDetaiDetailForm, final Model model) {
		userDetailViewService.setDetailInfo(userDetaiDetailForm, model);
	}

	/**
	 * 更新
	 * 
	 * @param userDetailUpdateForm 用户列表表单
	 */
	@ResponseBody
	@RequestMapping
	public void update(final UserDetailUpdateForm userDetailUpdateForm) {
		userDetailViewService.update(userDetailUpdateForm);
	}
}
